# Hashicorp Vault/Docker

Some small scripts and some documentation.

# Unlock

To use Hashicorp Vault, it needs to be unsealed after start up.
While installation (first start) it provides a JSON file.
Save it into ./private/vault-credentials.json and you
can use ./vault-unlock to unseal Hashicorp Vault.

# Docker Compose Healthcheck

Healthcheck checks *sealed" status. It changes to *healty* if vault is unlocked.
Locked vault yiedls into *unhealthy'

# Login

```
./vault login <root token>
```

# Database Admin User

Vault needs an admin user for database to rotate secrets.

```
./vault write database/config/mysql \
  plugin_name=mysql-database-plugin \
  connection_url="{{username}}:{{password}}@tcp(mysql)/" \
  allowed_roles="mysqlrole" \
  username="vault" \
  password="***"
```

Also it need to know how to create an database user:

```
./vault write database/roles/mysqlrole \
  db_name=mydatabase \
  creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}'; GRANT ALL PRIVILEGES ON mydatabase.* TO {{name}}'@'%';" \
  default_ttl="1h" \
  max_ttl="24h"
```

# Policy

Now we need a policy, what information may be retrieved by
a specific vault user/token created later with this policy.

```
./vault policy write mysql-policy - << EOF
path "database/creds/mysqlrole" {
  capabilities = ["read"]
}
EOF
```

should deliver a username/password pair for database.

# Generate Token

Now we can create a token which has limited access to vault.
**Never use root token for applications.**

```
./vault token create -policy=mysql-policy
```

# Test

```
./vault read database/creds/mysqlrole
```


