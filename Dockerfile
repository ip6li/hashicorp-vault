FROM debian:bookworm-slim

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt-utils
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

# Install Postfix.
run echo "postfix postfix/main_mailer_type string Internet site" > preseed.txt
run echo "postfix postfix/mailname string ${fqname}" >> preseed.txt
# Use Mailbox format.
run debconf-set-selections preseed.txt
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  postfix

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  procps \
  cron \
  curl \
  jq \
  lsb-release \
  gnupg2

ARG hostname=vault
ARG domain=example.com
ARG fqname=${hostname}.${domain}

RUN \
  groupadd --gid 90000 vault && \
  useradd -d /vault -g vault -s /bin/bash --uid 90000 vault

COPY ./mk-crt /usr/local/bin/mk-crt
RUN \
  chmod 755 /usr/local/bin/mk-crt

run \
  cd /tmp && \
  curl -LsS https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list && \
  DEBIAN_FRONTEND=noninteractive apt update && \
  DEBIAN_FRONTEND=noninteractive apt install -q -y vault

COPY ./entrypoint.sh /entrypoint.sh
RUN \
  chown vault:vault /entrypoint.sh && \
  chmod 700 /entrypoint.sh

USER vault
ENTRYPOINT [ "/entrypoint.sh" ]

