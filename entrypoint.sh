#!/usr/bin/env bash

if [ ! -f "/vault/cert.crt" ]; then
  cd /vault
  CRT_DAYS="3650" /usr/local/bin/mk-crt --rsa --mod 4096 --ip 127.0.0.1 --host vault.example.com vault.example.com
  mv vault.example.com.crt cert.crt
  mv vault.example.com.key cert.key
  cd ~
fi

/usr/bin/vault server -config /vault/vault.conf

#tail -f /dev/null

